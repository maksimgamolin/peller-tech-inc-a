import settings
import jinja2
import aiohttp_jinja2
from aiohttp import web


routes = web.RouteTableDef()


@routes.get('/')
async def hello(request):
    context = {'greet': 'Hello world!!!'}
    response = aiohttp_jinja2.render_template('base.html',
                                              request,
                                              context)
    response.headers['Content-Language'] = 'ru'
    return response


if __name__ == '__main__':
    app = web.Application()
    app.add_routes(routes)
    aiohttp_jinja2.setup(app,
                         loader=jinja2.FileSystemLoader(settings.TEMPLATE_DIR))
    web.run_app(app, host=settings.HOST, port=settings.PORT)
